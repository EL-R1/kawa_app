import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonCard,
    IonCardSubtitle,
    IonCardContent,
    IonCardHeader,
    IonCardTitle, IonSearchbar
} from '@ionic/react';
import './Tab1.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faRightFromBracket} from '@fortawesome/free-solid-svg-icons';
import cafe1 from '../assets/images/cafe1.jpg';
import cafe2 from '../assets/images/cafe2.jpg';
import cafe3 from '../assets/images/cafe3.jpg';
import cafe4 from '../assets/images/cafe4.jpg';
import FetchProducts from "../services/mock-data-service";
import React, {useState} from "react";

const Tab1: React.FC = () => {
    const [searchText, setSearchText] = useState('');
    const {data} = FetchProducts('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products');
    const images = [cafe1, cafe2, cafe3, cafe4];
    const randomIndex = Math.floor(Math.random() * images.length);
    const randomImage = images[randomIndex];
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage] = useState(5);

    //const indexOfLastItem = currentPage * itemsPerPage;
    //const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    //const currentItems = data?.slice(indexOfFirstItem, indexOfLastItem);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(data?.length / itemsPerPage); i++) {
        pageNumbers.push(i);
    }

    // Filter the products based on the search text
    const filteredProducts = data?.filter((product: any) => {
        return product.name.toLowerCase().includes(searchText.toLowerCase());
    });

    // Calculate the index of the first and last item to be displayed on the current page
    const indexOfLastItem = (currentPage + 1) * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;

    // Slice the filtered products array to display only the items for the current page
    const currentItems = filteredProducts?.slice(indexOfFirstItem, indexOfLastItem);

    // Calculate the total number of pages based on the number of items and the itemsPerPage value
    const totalPages = Math.ceil(filteredProducts?.length / itemsPerPage);

    // Handler function for changing the current page


    const handlePageChange = (newPage: number) => {
        setCurrentPage(newPage);
    };


    const paginationButtons = [];
    for (let i = 0; i < totalPages; i++) {
        paginationButtons.push(
            <button key={i} className={`pagination-button ${i === currentPage ? 'active' : ''}`} onClick={() => handlePageChange(i)}>
                {i + 1}
            </button>
        );
    }
    return (
        //TODO : recherche et limit data / plusieurs page
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Kawa app</IonTitle>
                    <FontAwesomeIcon icon={faRightFromBracket} />
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)} placeholder="Search for products" />
                {currentItems?.map((product: any) => (
                    <div key={product.id}>
                        <IonCard routerLink={`/DetailCafe/${product.id}`}>
                            <img alt="Cafe" src={randomImage}/>
                            <IonCardHeader>
                                <IonCardTitle>{product.name}</IonCardTitle>
                                <IonCardSubtitle>{product.details.description}</IonCardSubtitle>
                            </IonCardHeader>

                            <IonCardContent>
                                {product.details.price} €
                            </IonCardContent>
                        </IonCard>
                    </div>
                ))}
                <div className="pagination">
                    {paginationButtons}
                </div>
            </IonContent>
        </IonPage>
    );
};
export default Tab1;
