import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonCard,
    IonCardSubtitle,
    IonCardContent,
    IonCardHeader,
    IonCardTitle, IonIcon,
    IonFab,
    IonFabButton,
    IonButton
} from '@ionic/react';
import './DetailCafe.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faRightFromBracket} from '@fortawesome/free-solid-svg-icons';
import cafe1 from '../assets/images/cafe1.jpg';
import {camera, trash, close, arrowBack} from 'ionicons/icons';
import { prendrePhoto } from '../hooks/prendrePhoto';
import FetchProducts from "../services/mock-data-service";
import cafe2 from '../assets/images/cafe2.jpg';
import cafe3 from '../assets/images/cafe3.jpg';
import cafe4 from '../assets/images/cafe4.jpg';
import {useParams} from "react-router";
import { useState, useEffect } from "react";


interface RouteParams {
    id: string;
}

interface Product {
    id: string;
    name: string;
    details: {
      price: number;
      description: string;
      color: string;
    };
    stock: number;
    description: string;
    image: string;
}

const DetailCafe: React.FC = () => {
    const { id } = useParams<RouteParams>();
    const [product, setProduct] = useState<Product>({
        id: "",
        name: "",
        details: {
            price: 0,
            description: "",
            color: "",
        },
        stock: 0,
        description: "",
        image: "",
    });

    useEffect(() => {
        const fetchProduct = async () => {
            try {
                const response = await fetch(`https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/${id}`);
                const data = await response.json();
                setProduct(data);
            } catch (error) {
                console.error(error);
            }
        };
        fetchProduct();
    }, [id]);

    const { takePhoto } = prendrePhoto();
    const images = [cafe1, cafe2, cafe3, cafe4];
    const randomIndex = Math.floor(Math.random() * images.length);
    const randomImage = images[randomIndex];
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Kawa app</IonTitle>
                    <FontAwesomeIcon icon={faRightFromBracket} />
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonButton routerLink={'/tab1'}>
                    <IonIcon icon={arrowBack}></IonIcon>
                </IonButton>
                <IonCard>
                    <img alt="Cafe1" src={randomImage}/>
                    <IonCardHeader>
                        <IonCardTitle>{product.name}</IonCardTitle>
                        <IonCardSubtitle>{product.details.description}</IonCardSubtitle>
                    </IonCardHeader>

                    <IonCardContent>
                        {product.details.price} - {product.stock} - {product.details.color}
                    </IonCardContent>
                </IonCard>
                <IonFab horizontal="center">
                    <IonFabButton onClick={() => takePhoto()}>
                    <IonIcon icon={camera}></IonIcon>
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>

    );
};

export default DetailCafe;
