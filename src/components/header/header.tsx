import {IonTitle, IonToolbar} from "@ionic/react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faRightFromBracket} from "@fortawesome/free-solid-svg-icons";

const Header: React.FC = () => {
    return (
        <IonToolbar>
            <IonTitle>Kawa app</IonTitle>
            <FontAwesomeIcon icon={faRightFromBracket} />
        </IonToolbar>
    );
};

export default Header;
